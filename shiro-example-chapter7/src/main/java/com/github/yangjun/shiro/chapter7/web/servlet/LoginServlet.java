package com.github.yangjun.shiro.chapter7.web.servlet;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.IncorrectCredentialsException;
import org.apache.shiro.authc.UnknownAccountException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.subject.Subject;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * Author: Jun.Yang
 * Create: Jun.Yang (2015/10/10)
 * Description:
 * To change this template use File | Settings | File Templates.
 */
@WebServlet(name = "loginServlet", urlPatterns = "/login")
public class LoginServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
       req.getRequestDispatcher("/WEB-INF/jsp/login.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setCharacterEncoding("UTF-8");
        String error = null;
        String username = req.getParameter("username");
        String password = req.getParameter("password");
        Subject subject = SecurityUtils.getSubject();
        UsernamePasswordToken token = new UsernamePasswordToken(username, password);
        token.setRememberMe(true);
        resp.setCharacterEncoding("UTF-8");

        try {
            subject.login(token);
        }catch (UnknownAccountException e) {
            error = "用户名/密码错误";
        }catch (IncorrectCredentialsException e) {
            error = "用户名/密码错误";
        }catch (AuthenticationException e) {
            error = "其他错误：" + e.getMessage();
        }

        if(error != null) {
            req.setAttribute("error", error);
            req.getRequestDispatcher("/WEB-INF/jsp/login.jsp").forward(req,resp);
        }else {
            req.setAttribute("subject", subject);
            req.getRequestDispatcher("/WEB-INF/jsp/loginSuccess.jsp").forward(req,resp);
        }

    }
}
